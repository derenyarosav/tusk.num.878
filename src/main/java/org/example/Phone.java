package org.example;

public abstract class Phone {
private int memory;
private String model;
private int power;
private int year;

   public Phone(int memory, String model, int power, int year){
       this.memory = memory;
       this.model = model;
       this.power  = power;
       this.year = year;
   }
public  String toString() {
return "memory : " + memory + " "+ "model : " + model + " " + "power : " + power +" " + "year : "+ year;
}
    }
